function rgbPeaks = overlayPeaks(idxs, data, rgbPeakColor)
% Assume 2D data corresponds to a mask
if ~isvector(idxs)
  % Dilate for visibility
  idxs = imdilate(idxs, ones(3));
  idxs = find(idxs);
end
if size(data, 3) < 3
  data = repmat(data, 1,1,3);
end
if nargin < 3
  rgbPeakColor = [255 0 0];
end
matsz = size(data,1)*size(data,2);

rgbPeaks = data;
rgbPeaks(idxs) = rgbPeakColor(1);
rgbPeaks(idxs + matsz) = rgbPeakColor(2);
rgbPeaks(idxs + 2*matsz) = rgbPeakColor(3);
end