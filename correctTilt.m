function img = correctTilt(img, chipOutline, printfig)
% chipOutline = imdilate(chipOutline, ones(3));
[H,T,R] = hough(chipOutline,'RhoResolution',0.5,'Theta',-45:0.5:45);

% Make temporary figure to show plot of hough transform when needed
f = figure('visible', 'off');
imshow(imadjust(rescale(H)),'XData',T,'YData',R,...
  'InitialMagnification','fit');
title('Hough transform of image');
xlabel('\theta'), ylabel('\rho');
axis on, axis normal, hold on;
colormap(gca,hot);
printfig(export_fig(), 'houghMap.png');
close(f);
% Only show values close to max
% Only keep top 20% of values
[~, thetaVal] = find(H == max(H(:)));

chipTilt = -T(thetaVal);
% Unwarp the image
tform = affine2d([cosd(chipTilt) sind(chipTilt) 0;...
  -sind(chipTilt) cosd(chipTilt) 0; 0 0 1]);
img = imwarp(img, tform);
end