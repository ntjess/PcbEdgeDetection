function chipMask = bwChipMask(filImg, printfig)
sobX = [-1 0 1; -2 0 2; -1 0 1];
sob = sobX + 1j*sobX';

% First, find edges
edges = (conv2(filImg, sob, 'valid'));
% Only keep the magnitude of derivative
edges = abs(edges);
% Pad output so it's the same size as the original image
edges = padarray(edges, [1 1]);
printfig(uint8(rescale(edges)*255), 'prelimEdges.png');

% We know the package edges constitute the largest derivative, so only
% look for strong edges in the image and assume they belong to the
% package boundary
%     pkgBounds = edges > max(edges(:))*0.25;
relevantPkgEdges = edges > max(edges(:))*0.1;
printfig(relevantPkgEdges, 'relevantEdges.png');

comps = bwconncomp(relevantPkgEdges);
% Only keep the largest connected component, since we assume this is the
% package boundary
compsize = cellfun('length', comps.PixelIdxList);
outlinePixs = comps.PixelIdxList{compsize == max(compsize)};

% Draw a bounding box around outline pixels
chipMask = false(size(relevantPkgEdges));
chipMask(outlinePixs) = true;
chipMask = bwconvhull(chipMask);
printfig(chipMask, 'chipMask.png');
end